# PySpark Fundamentals



## PreReqs

### Self Service
```
Python
JDK 8 - Any JDK should work
Docker
```
### Terminal/CLI
```
pip3 install pyspark
```
Running any python file, use the cmd `python3 filename` if a missing library error appears you can `pip3 install library` to fix.

## Basics
Run through some basic commands that will be used in almost every piece of code you'll create. 

You'll find tutorials with detailed descriptions of each command and a thorough walkthrough of all the basics here: https://sparkbyexamples.com/ 
## Docker
Starting the Docker Container
```sh
./docker_setup.sh
```
Removing the Docker Container once finished
```sh
./remove_container.sh
```



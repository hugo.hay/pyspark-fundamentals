from sparksession import spark_session, stop_session
from pyspark.sql.functions import col, lit, when, regexp_replace

session = spark_session()

print("Reading Data in")
transform_df = session.read.option("multiline","true").json("resources/json/json_valid.json")
print("Creating a view to run SQL commands")
transform_df.createOrReplaceTempView("test_data")

print("Showing a DataFrame")
transform_df.show()
session.sql("SELECT * FROM test_data").show()

print("Adding a New Column")
transform_df = transform_df.withColumn("salary", lit(1500).cast("Double"))

print("Updating all Rows with a String Value")
transform_df.withColumn("city", lit("utopia")).show()
# PySpark SQL doesn't support UPDATE queries

print("Updating Integer/Double Values")
transform_df.withColumn("salary", lit(100).cast("Integer")).show()
transform_df.withColumn(\
    "salary",\
     when(col("age") >= 15,\
          col("salary")*1.4)\
              .otherwise(col("salary") * 0.05))\
                  .show()

# Fixing the -1
transform_df = transform_df.withColumn("age", when(transform_df.age == -1,52)\
                                            .otherwise(transform_df.age))



print("RegEx Replace")
transform_df = transform_df.withColumn('city', regexp_replace('city', 'BHAM', 'Birmingham'))
# Fixing Em32ily
transform_df = transform_df.withColumn("name", \
    regexp_replace(transform_df.name, "[0-9]", ""))
transform_df.show()

transform_df.write.json("resources/output/fixed_data")

stop_session(session)

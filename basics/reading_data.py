from sparksession import spark_session, stop_session

session = spark_session()

# ==========================
# =         JSON           =
# ==========================
print("Reading a multiline JSON file")
json_df = session.read.option("multiline","true").json("resources/json/json_valid.json")
print("Without multiline set to true this would fail")
json_df.show()

print("Spark writes JSON in a line by line format - Using multiline here only takes the first line")
json_df = session.read.option("multiline","true").json("resources/json/spark_valid.json")
json_df.show()
print("Removing Multiline will read all records")
json_df = session.read.json("resources/json/spark_valid.json")
json_df.show()

print("Important distinction when using JSON Files")
# ==========================
# =          CSV           =
# ==========================
print("Reading CSV files")
csv_df = session.read.csv("resources/csv/example.csv")
csv_df.show()
print("You'll notice the column headers aren't assigned values")
csv_df = session.read.option("header",True).csv("resources/csv/example.csv")
csv_df.show()
print("The columns are now named")

stop_session(session)
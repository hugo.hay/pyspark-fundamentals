from sparksession import spark_session, stop_session

session = spark_session()

print("Reading Data in")
output_df = session.read.option("multiline","true").json("resources/json/json_valid.json")
# ==========================
# =         JSON           =
# ==========================
print("Writing JSON")
output_df.write.json("resources/output/json")
# ==========================
# =          CSV           =
# ==========================
print("Writing CSV")
output_df.write.csv("resources/output/csv")
# ==========================
# =        Parquet         =
# ==========================
print("Writing Parquet")
output_df.write.parquet("resources/output/parquet")

stop_session(session)

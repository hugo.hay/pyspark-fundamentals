from pyspark.sql import SparkSession

def spark_session():
    return SparkSession.builder.appName("pyspark-fundamentals").getOrCreate()
def stop_session(session):
    session.stop()
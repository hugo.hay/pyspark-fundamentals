# =====================================================================================================================
# Variables - Change Port if Occupied
# =====================================================================================================================
PORT=8888
# =====================================================================================================================
# Init the container
# =====================================================================================================================
echo "Starting container..."
docker run -itd -p $PORT:$PORT -e DISABLE_SSL=TRUE --name pyspark-notebook jupyter/pyspark-notebook
echo "Container Running"
# =====================================================================================================================
# Jupyter
# =====================================================================================================================
echo "Starting Jupyter..."
docker exec -d pyspark-notebook sh -c "/home/jupyter/jupyter_start.sh"
echo "Jupyter Started"
# =====================================================================================================================
# Copying Files Across
# Update with any files you want to add:
# docker cp filepath pyspark-notebook:/home/jovyan
# =====================================================================================================================
echo "Copying Notebook(s)..."
docker cp resources/fundamentals.ipynb pyspark-notebook:/home/jovyan
echo "Copied"
echo "Copying File Data..."
docker cp resources/test.json pyspark-notebook:/home/jovyan
echo "Copied"
# =====================================================================================================================
# Your token to login
# =====================================================================================================================
echo "Loading Jupyter..."
sleep 3
echo "\nGetting Jupyter Token"
server_list=$(docker exec -it pyspark-notebook jupyter server list)
TOKEN=${server_list% ::*}  # retain the part before the token ends
TOKEN=${TOKEN##*token=}  # retain the part after the token=
echo $TOKEN | pbcopy
echo "Token Copied to your clipboard"
# =====================================================================================================================
# Opening Localhost
# =====================================================================================================================
echo "Opening http://localhost:$PORT/?token=$TOKEN" 
open http://localhost:$PORT/?token=$TOKEN
echo "Jupyter Loaded"
